# passerelle-montpellier-encombrants
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from .models import Commune, EncombrantsManagement, Sector, Street


class ListValidator:
    def __init__(self, item_validator):
        self.item_validator = item_validator

    def __call__(self, value):
        for i, item in enumerate(value):
            try:
                self.item_validator(item)
            except ValidationError as e:
                raise


class CommaSeparatedEmailField(forms.Field):
    def __init__(self, dedup=True, max_length=None, min_length=None, *args, **kwargs):
        self.dedup = dedup
        self.max_length = max_length
        self.min_length = min_length
        item_validators = [validators.EmailValidator()]
        super().__init__(*args, **kwargs)
        for item_validator in item_validators:
            self.validators.append(ListValidator(item_validator))

    def to_python(self, value):
        if value in validators.EMPTY_VALUES:
            return []

        value = [item.strip() for item in value.split(',') if item.strip()]
        if self.dedup:
            value = list(set(value))
        return value

    def clean(self, value):
        values = self.to_python(value)
        self.validate(values)
        self.run_validators(values)
        return value


class EncombrantsManagementForm(forms.ModelForm):
    class Meta:
        model = EncombrantsManagement
        exclude = ('slug', 'users')

    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = slugify(self.instance.title)
        return super().save(commit=commit)


class EncombrantsManagementUpdateForm(EncombrantsManagementForm):
    class Meta:
        model = EncombrantsManagement
        exclude = ('users',)


class NoStreetsCommuneForm(forms.ModelForm):
    class Meta:
        model = Commune
        exclude = ('streets',)


class CommuneForm(forms.ModelForm):
    streets = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 25, 'rows': 10}),
        help_text=_('one street by line'),
        required=False,
    )

    class Meta:
        model = Commune
        fields = '__all__'

    def save(self):
        obj = super().save()
        if self.cleaned_data['streets']:
            streets = self.cleaned_data['streets'].split('\n')
            for street in streets:
                if street.strip():
                    Street.objects.create(commune=obj, name=street.strip())
        return obj


class SectorForm(forms.ModelForm):
    contact_email = CommaSeparatedEmailField(
        label=_('Emails'), required=False, help_text=_('separated by commas')
    )

    class Meta:
        model = Sector
        fields = '__all__'


class StreetsForm(forms.Form):
    streets = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 25, 'rows': 10}),
        help_text=_('one street by line'),
        required=False,
    )
