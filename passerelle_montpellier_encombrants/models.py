# passerelle-montpellier-encombrants
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.db import models
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint

from .utils import get_sector


class EncombrantsManagement(BaseResource):
    category = _('Business Process Connectors')

    class Meta:
        verbose_name = 'Gestion des encombrants Montpellier'

    manager_view_template_name = 'passerelle_montpellier_encombrants/detail.html'

    @endpoint(
        perm='can_access',
        pattern=r'^(?P<insee>\w+)$',
        parameters={
            'adresse': {'description': _('Address')},
            'limit': {'description': _('Results number limit')},
        },
    )
    def available(self, request, insee, adresse=None, limit=None):
        sector = get_sector(insee, adresse)
        if sector:
            if datetime.datetime.today().weekday() == 4:
                tomorrow = datetime.datetime.today() + datetime.timedelta(days=3)
            elif datetime.datetime.today().weekday() == 5:
                tomorrow = datetime.datetime.today() + datetime.timedelta(days=2)
            else:
                tomorrow = datetime.datetime.today() + datetime.timedelta(days=1)
            collect_days = sector.collectday_set.filter(date__gt=tomorrow).order_by('date')
        else:
            collect_days = []
        result = [
            {'id': x.date.strftime('%Y-%m-%d'), 'text': x.date.strftime('%d/%m/%Y')} for x in collect_days
        ]
        if limit is not None and limit.isnumeric():
            result = result[: int(limit)]
        return {'data': result}


class Sector(models.Model):
    contact_email = models.CharField(
        max_length=1024, blank=True, verbose_name=_('Contact Emails'), help_text=_('separated by commas')
    )

    class Meta:
        verbose_name = _('Sector')

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    @property
    def title(self):
        return self

    def __str__(self):
        communes = Commune.objects.filter(sector=self)
        if not communes:
            return '(%s)' % self.id
        return ', '.join([x.name for x in communes])


class Commune(models.Model):
    name = models.CharField(max_length=50, blank=False, verbose_name=_('Name'))
    insee = models.CharField(max_length=10, blank=False, verbose_name=_('INSEE Code'))
    sector = models.ForeignKey(Sector, verbose_name=_('Sector'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Commune')

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    @property
    def title(self):
        return self

    def __str__(self):
        return '%s (%s)' % (self.name, self.insee)


class Street(models.Model):
    commune = models.ForeignKey(Commune, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, blank=False, verbose_name=_('Street'))

    def __str__(self):
        return '%s, %s' % (self.name, self.commune)


class CollectDay(models.Model):
    sector = models.ForeignKey(Sector, verbose_name=_('Sector'), on_delete=models.CASCADE)
    date = models.DateField(verbose_name=_('Date'))

    class Meta:
        verbose_name = _('Collect day')

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    @property
    def title(self):
        return self

    class Meta:
        ordering = ['date']
        unique_together = ('sector', 'date')

    def __str__(self):
        return '%s: %s' % (self.date, self.sector)
