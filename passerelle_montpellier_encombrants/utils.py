# passerelle-montpellier-encombrants
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from collections import defaultdict
from io import BytesIO

from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.utils.translation import gettext_lazy as _

from .ods import Workbook

prefix_map = {
    'ALL': 'ALLEE',
    'AUTO': 'AUTOROUTE',
    'AV': 'AVENUE',
    'BASS': 'BASSIN',
    'BD': 'BOULEVARD',
    'CAR': 'CARREFOUR',
    'CHE': 'CHEMIN',
    'COUR': 'COUR',
    'CRS': 'COURS',
    'DESC': 'DESCENTE',
    'DOM': 'DOMAINE',
    'ENCL': 'ENCLOS',
    'ESP': 'ESPLANADE',
    'ESPA': 'ESPACE',
    'GR': '',  # "GR GRAND-RUE JEAN MOULIN"
    'IMP': 'IMPASSE',
    'JARD': 'JARDIN',
    'MAIL': '',  # "MAIL LE GRAND MAIL"
    'PARC': 'PARC',
    'PARV': '',  # "PARV PARVIS DE LA LEGION D HONNEUR"
    'PAS': 'PASSAGE',
    'PL': 'PLACE',
    'PLAN': 'PLAN',
    'PONT': 'PONT',
    'QUA': 'QUAI',
    'R': 'RUE',
    'RAMB': '',  # "RAMB RAMBLA DES CALISSONS"
    'RPT': 'ROND-POINT',
    'RTE': 'ROUTE',
    'SQ': 'SQUARE',
    'TSSE': '',  # "TSSE TERRASSE DES ALLEES DU BOIS"
    'TUN': 'TUNNEL',
    'VIAD': 'VIADUC',
    'VOI': 'VOIE',
}


def prefix_cleanup(name):
    name = name.strip()
    for prefix, full in prefix_map.items():
        if name.startswith(prefix + ' '):
            name = (full + name[len(prefix) :]).strip()
    return name


def get_sector(insee, address=None):
    from .models import Commune

    communes = Commune.objects.filter(insee=insee)
    for commune in communes:
        streets = commune.street_set.all()
        if address and streets:
            for s in streets:
                if prefix_cleanup(s.name) in address.upper():
                    return commune.sector
        else:
            return commune.sector


def email_sectors(formdatas, when):
    subject = get_template('passerelle_montpellier_encombrants/email_subject.txt')
    message = get_template('passerelle_montpellier_encombrants/email_body.txt')

    header = ('Commune', 'Prénom', 'Nom', 'Numero', 'Voie', 'Date', 'Téléphone', 'Commentaire')

    sectors = defaultdict(list)

    context = {'date': when}
    for data in formdatas:
        fields = data['fields']

        data = [
            fields.get(d) or ''
            for d in ('commune', 'prenom', 'nom', 'numero', 'voie', 'date', 'telephone', 'commentaire')
        ]

        sector = get_sector(fields.get('commune_raw'), fields.get('adresse'))
        if sector:
            sectors[sector.contact_email].append(data)

    for destination, data in sectors.items():
        destinations = [d.strip() for d in destination.split(',') if d.strip()]
        mail = EmailMessage(
            subject.render(context).strip(),
            message.render(context),
            settings.DEFAULT_FROM_EMAIL,
            destinations,
        )
        attachement_body = BytesIO()

        # create ods
        ods = Workbook(encoding='utf-8')
        ws = ods.add_sheet('demandes')
        for i, e in enumerate(header):
            ws.write(0, i, e)

        # sort by "commune" and "voie" fields
        data.sort(key=lambda e: (e[0], e[4]))

        for i, d in enumerate(data):
            for j, e in enumerate(d):
                ws.write(i + 1, j, e)

        ods.save(attachement_body)

        mail.attach(
            'demandes.ods', attachement_body.getvalue(), 'application/vnd.oasis.opendocument.spreadsheet'
        )
        mail.send()
