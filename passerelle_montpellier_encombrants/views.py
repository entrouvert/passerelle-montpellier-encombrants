# passerelle-montpellier-encombrants
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.db import transaction
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView, ListView
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from passerelle import utils as passerelle_utils

from .forms import (
    CommuneForm,
    EncombrantsManagementForm,
    EncombrantsManagementUpdateForm,
    NoStreetsCommuneForm,
    SectorForm,
    StreetsForm,
)
from .models import CollectDay, Commune, EncombrantsManagement, Sector, Street
from .utils import get_sector, prefix_cleanup


class SectorListView(ListView):
    model = Sector

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = EncombrantsManagement.objects.all()[0]
        return context


class SectorCreateView(CreateView):
    model = Sector
    form_class = SectorForm
    template_name = 'passerelle/manage/service_form.html'
    success_url = reverse_lazy('montpellier-encombrants-sector-listing')


class SectorUpdateView(UpdateView):
    model = Sector
    form_class = SectorForm
    template_name = 'passerelle/manage/service_form.html'
    success_url = reverse_lazy('montpellier-encombrants-sector-listing')


class SectorDeleteView(DeleteView):
    model = Sector
    template_name = 'passerelle/manage/service_confirm_delete.html'
    success_url = reverse_lazy('montpellier-encombrants-sector-listing')


class CommuneListView(ListView):
    model = Commune

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = EncombrantsManagement.objects.all()[0]
        return context


class CommuneCreateView(CreateView):
    model = Commune
    form_class = CommuneForm
    template_name = 'passerelle/manage/service_form.html'
    success_url = reverse_lazy('montpellier-encombrants-commune-listing')


class CommuneView(DetailView):
    model = Commune

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = EncombrantsManagement.objects.all()[0]
        context['commune'] = self.object
        context['streets'] = self.object.street_set.all()
        return context


class CommuneUpdateView(UpdateView):
    model = Commune
    form_class = NoStreetsCommuneForm
    template_name = 'passerelle/manage/service_form.html'
    success_url = reverse_lazy('montpellier-encombrants-commune-listing')


class CommuneDeleteView(DeleteView):
    model = Commune
    template_name = 'passerelle/manage/service_confirm_delete.html'
    success_url = reverse_lazy('montpellier-encombrants-commune-listing')


class CollectDayListView(ListView):
    model = CollectDay

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = EncombrantsManagement.objects.all()[0]
        return context


class CollectDayCreateView(CreateView):
    model = CollectDay
    template_name = 'passerelle/manage/service_form.html'
    success_url = reverse_lazy('montpellier-encombrants-collectday-listing')
    fields = '__all__'


class CollectDayDeleteView(DeleteView):
    model = CollectDay
    template_name = 'passerelle/manage/service_confirm_delete.html'
    success_url = reverse_lazy('montpellier-encombrants-collectday-listing')


class StreetEditView(FormView):
    form_class = StreetsForm
    template_name = 'passerelle/manage/service_form.html'

    def get_success_url(self):
        return reverse('montpellier-encombrants-commune-view', kwargs={'pk': self.kwargs['pk']})

    def get_initial(self):
        try:
            c = Commune.objects.get(pk=self.kwargs['pk'])
            streets = c.street_set.all()
            if streets:
                return {'streets': '\n'.join([s.name for s in streets])}
        except Commune.DoesNotExist:
            pass

    def form_valid(self, form):
        try:
            c = Commune.objects.get(pk=self.kwargs['pk'])
            with transaction.atomic():
                c.street_set.all().delete()
                for s in form.cleaned_data.get('streets', '').split('\n'):
                    s = s.strip()
                    if s:
                        Street.objects.create(commune=c, name=s)
        except Commune.DoesNotExist:
            pass
        return super().form_valid(form)
