import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0004_auto_20150430_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Street',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=128, verbose_name='Street')),
                (
                    'commune',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='passerelle_montpellier_encombrants.Commune',
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
