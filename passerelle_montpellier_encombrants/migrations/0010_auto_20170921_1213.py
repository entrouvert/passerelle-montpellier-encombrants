from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0009_encombrantsmanagement_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='encombrantsmanagement',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
