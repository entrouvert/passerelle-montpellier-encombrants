import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0003_sector_contact_email'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sector',
            options={'verbose_name': 'Sector'},
        ),
        migrations.AlterField(
            model_name='collectday',
            name='date',
            field=models.DateField(verbose_name='Date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='collectday',
            name='sector',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                verbose_name='Sector',
                to='passerelle_montpellier_encombrants.Sector',
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='commune',
            name='insee',
            field=models.CharField(max_length=10, verbose_name='INSEE Code'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='commune',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='commune',
            name='sector',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                verbose_name='Sector',
                to='passerelle_montpellier_encombrants.Sector',
            ),
            preserve_default=True,
        ),
    ]
