from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0008_auto_20151201_0440'),
    ]

    operations = [
        migrations.AddField(
            model_name='encombrantsmanagement',
            name='log_level',
            field=models.CharField(
                default=b'NOTSET',
                max_length=10,
                verbose_name='Log Level',
                choices=[
                    (b'NOTSET', b'NOTSET'),
                    (b'DEBUG', b'DEBUG'),
                    (b'INFO', b'INFO'),
                    (b'WARNING', b'WARNING'),
                    (b'ERROR', b'ERROR'),
                    (b'CRITICAL', b'CRITICAL'),
                    (b'FATAL', b'FATAL'),
                ],
            ),
        ),
    ]
