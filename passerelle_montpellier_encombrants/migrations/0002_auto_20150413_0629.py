import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CollectDay',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('date', models.DateField()),
            ],
            options={
                'ordering': ['date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Commune',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=50)),
                ('insee', models.CharField(max_length=10)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='commune',
            name='sector',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='passerelle_montpellier_encombrants.Sector'
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='collectday',
            name='sector',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='passerelle_montpellier_encombrants.Sector'
            ),
            preserve_default=True,
        ),
    ]
