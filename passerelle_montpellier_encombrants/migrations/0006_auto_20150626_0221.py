from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0005_street'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='collectday',
            unique_together={('sector', 'date')},
        ),
    ]
