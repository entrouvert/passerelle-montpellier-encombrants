from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0007_auto_20150915_1014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sector',
            name='contact_email',
            field=models.CharField(
                help_text='separated by commas', max_length=1024, verbose_name='Contact Emails', blank=True
            ),
            preserve_default=True,
        ),
    ]
