from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_montpellier_encombrants', '0002_auto_20150413_0629'),
    ]

    operations = [
        migrations.AddField(
            model_name='sector',
            name='contact_email',
            field=models.EmailField(max_length=75, verbose_name='Contact Email', blank=True),
            preserve_default=True,
        ),
    ]
