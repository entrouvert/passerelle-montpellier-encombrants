# passerelle-montpellier-encombrants
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.decorators import login_required
from django.urls import include, path, re_path
from passerelle.urls_utils import app_enabled, decorated_includes, required

from .views import *

management_urlpatterns = [
    path('sectors/', SectorListView.as_view(), name='montpellier-encombrants-sector-listing'),
    path('sectors/add', SectorCreateView.as_view(), name='montpellier-encombrants-sector-add'),
    re_path(
        r'^sectors/(?P<pk>[\w,-]+)/$',
        SectorUpdateView.as_view(),
        name='montpellier-encombrants-sector-update',
    ),
    re_path(
        r'^sectors/(?P<pk>[\w,-]+)/delete$',
        SectorDeleteView.as_view(),
        name='montpellier-encombrants-sector-remove',
    ),
    path('communes/', CommuneListView.as_view(), name='montpellier-encombrants-commune-listing'),
    path('communes/add', CommuneCreateView.as_view(), name='montpellier-encombrants-commune-add'),
    re_path(
        r'^communes/(?P<pk>[\w,-]+)/edit$',
        CommuneUpdateView.as_view(),
        name='montpellier-encombrants-commune-edit',
    ),
    re_path(
        r'^communes/(?P<pk>[\w,-]+)$', CommuneView.as_view(), name='montpellier-encombrants-commune-view'
    ),
    re_path(
        r'^communes/(?P<pk>[\w,-]+)/streets$',
        StreetEditView.as_view(),
        name='montpellier-encombrants-commune-streets-edit-view',
    ),
    re_path(
        r'^communes/(?P<pk>[\w,-]+)/delete$',
        CommuneDeleteView.as_view(),
        name='montpellier-encombrants-commune-remove',
    ),
    path('collectdays/', CollectDayListView.as_view(), name='montpellier-encombrants-collectday-listing'),
    path('collectdays/add', CollectDayCreateView.as_view(), name='montpellier-encombrants-collectday-add'),
    re_path(
        r'^collectdays/(?P<pk>[\w,-]+)/delete$',
        CollectDayDeleteView.as_view(),
        name='montpellier-encombrants-collectday-remove',
    ),
]
