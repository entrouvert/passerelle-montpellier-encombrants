import datetime
import json
from random import randint

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.utils.encoding import force_str
from passerelle.base.models import AccessRight, ApiUser

from passerelle_montpellier_encombrants.models import (
    CollectDay,
    Commune,
    EncombrantsManagement,
    Sector,
    Street,
)

maurin_streets = (
    'CHE DES JARDINS DE MAGUELONE',
    'RTE DEPARTEMENTALE 132',
    'CHE DE SAINT-HUBERT',
    'PLAN DU MAS DE SARDAN',
    'R DES ROBINIERS',
)

lattes_streets = (
    'AV DE BOIRARGUES',
    'RPT DE LA FONTVIN',
    'PLAN ROSSINI',
    'R DES CYCLAMENS',
    'RTE DEPARTEMENTALE 172',
    'CHE DU MAS DE CAUSSE À L\'ESTELLE',
)


class EncombrantsTestCase(TestCase):
    fixtures = []

    def setUp(self):
        self.client = Client()
        self.instance = EncombrantsManagement.objects.create(
            title='montpellier', slug='montpellier', description='encombrants'
        )
        api, _ = ApiUser.objects.get_or_create(username='all', keytype='', key='')
        obj_type = ContentType.objects.get_for_model(self.instance)
        AccessRight.objects.update_or_create(
            codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=self.instance.pk
        )
        self.now = datetime.datetime.now()
        for mail, insee, name in (
            ('cournonsec@montpellier3m.fr', '34087', 'Cournonsec'),
            ('jacou@montpellier3m.fr', '34120', 'Jacou'),
            ('castelnau@montpellier3m.fr', '34057', 'Castelnau'),
            ('prades@montpellier3m.fr', '34217', 'Prades'),
            ('sussargues@montpellier3m.fr', '34307', 'Sussargues'),
        ):
            sector = Sector.objects.create(contact_email=mail)
            commune = Commune.objects.create(sector=sector, insee=insee, name=name)
            for i in range(2, randint(3, 10)):
                when = self.now + datetime.timedelta(days=i)
                CollectDay.objects.create(sector=sector, date=when)

    def test_collectdays(self):
        for commune in Commune.objects.all():
            r = self.client.get(
                reverse(
                    'generic-endpoint',
                    kwargs={
                        'connector': 'passerelle-montpellier-encombrants',
                        'endpoint': 'available',
                        'slug': self.instance.slug,
                        'rest': commune.insee,
                    },
                )
            )
            data = json.loads(force_str(r.content))

    def test_collectdays_in_communes_with_street(self):
        maurin_sector = Sector.objects.create(contact_email='maurin@montpellier3m.fr')
        maurin_commune = Commune.objects.create(sector=maurin_sector, name='Maurin', insee='34970')

        lattes_sector = Sector.objects.create(contact_email='lattes@montpellier3m.fr')
        lattes_commune = Commune.objects.create(sector=lattes_sector, name='Lattes', insee='34970')

        for s in maurin_streets:
            Street.objects.create(commune=maurin_commune, name=s)

        CollectDay.objects.create(sector=maurin_sector, date=self.now + datetime.timedelta(days=2))

        # Maurin
        r = self.client.get(
            reverse(
                'generic-endpoint',
                kwargs={
                    'connector': 'passerelle-montpellier-encombrants',
                    'endpoint': 'available',
                    'slug': self.instance.slug,
                    'rest': '34970',
                },
            ),
            {'adresse': 'PLAN DU MAS DE SARDAN'},
        )
        data = json.loads(force_str(r.content))
        # self.assertNotEqual(data['data'], [])

        # Lattes
        for s in lattes_streets:
            Street.objects.create(commune=lattes_commune, name=s)

        r = self.client.get(
            reverse(
                'generic-endpoint',
                kwargs={
                    'connector': 'passerelle-montpellier-encombrants',
                    'endpoint': 'available',
                    'slug': self.instance.slug,
                    'rest': '34970',
                },
            ),
            {'adresse': 'ROUTE DEPARTEMENTALE 172'},
        )
        data = json.loads(force_str(r.content))
        # self.assertEqual(data['data'], [])

        CollectDay.objects.create(sector=lattes_sector, date=self.now + datetime.timedelta(days=4))

        r = self.client.get(
            reverse(
                'generic-endpoint',
                kwargs={
                    'connector': 'passerelle-montpellier-encombrants',
                    'endpoint': 'available',
                    'slug': self.instance.slug,
                    'rest': '34970',
                },
            ),
            {'adresse': 'ROUTE DEPARTEMENTALE 172'},
        )
        data = json.loads(force_str(r.content))
        self.assertNotEqual(data['data'], [])

    def test_collectdays_nonexisting_street(self):
        r = self.client.get(
            reverse(
                'generic-endpoint',
                kwargs={
                    'connector': 'passerelle-montpellier-encombrants',
                    'endpoint': 'available',
                    'slug': self.instance.slug,
                    'rest': '34970',
                },
            ),
            {'adresse': 'Nonexisting street'},
        )
        data = json.loads(force_str(r.content))
        self.assertEqual(data['data'], [])

    def test_collectdays_random_case_street(self):
        lattes_sector = Sector.objects.create(contact_email='lattes@montpellier3m.fr')
        lattes_commune = Commune.objects.create(sector=lattes_sector, name='Lattes', insee='34970')

        for s in lattes_streets:
            Street.objects.create(commune=lattes_commune, name=s)

        CollectDay.objects.create(sector=lattes_sector, date=self.now + datetime.timedelta(days=10))

        r = self.client.get(
            reverse(
                'generic-endpoint',
                kwargs={
                    'connector': 'passerelle-montpellier-encombrants',
                    'endpoint': 'available',
                    'slug': self.instance.slug,
                    'rest': '34970',
                },
            ),
            {'adresse': 'route departementale 172'},
        )
        data = json.loads(force_str(r.content))
        self.assertNotEqual(data['data'], [])
